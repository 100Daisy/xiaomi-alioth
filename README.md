# Poco F3 

## Current state:
- Unstable

### Working:
- Display
- Touchscreen
- RIL
- Wifi
- Flashlight
- Bluetooth
- NFC

### Broken:
Audio
Screenshot
Camera
Fingerprint